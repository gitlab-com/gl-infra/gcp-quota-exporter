FROM golang:1.21.3-alpine as alpine

RUN apk add --no-cache git ca-certificates make

FROM scratch
COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY gcp-quota-exporter /app/gcp-quota-exporter
WORKDIR /app
EXPOSE 9592
ENTRYPOINT ["./gcp-quota-exporter"]
