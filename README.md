# Google Cloud Platform Quota Exporter

Exports limits and usage for metrics available through the GCP APIs (currently only supports Compute Engine).

## Usage

1. Set up a service account in the project you wish to monitor. The account should be given the following permissions:
  * compute.projects.get
  * compute.regions.list
1. Authentication is performed using the standard [Application Default Credentials](https://developers.google.com/accounts/docs/application-default-credentials)
  * To use a credentials.json key file export the environment variable `GOOGLE_APPLICATION_CREDENTIALS=path-to-credentials.json`
1. The exporter needs to know which projects to monitor quotas for
  * Specify project using `--gcp.project_id`. Specify this flag multiple times to poll additional projects
  * Export environment variable `GOOGLE_PROJECT_ID`, newline separated entries can be provided for polling multiple projects
  * Fetch from compute metadata `http://metadata.google.internal/computeMetadata/v1/project/project-id`
1. Optionally provide a list of regions to collect quota metrics for.
  * Specify a region using `--gcp.region`. Specify this flag multiple times to collect additional regions
  * Export environment variable `GOOGLE_REGION`, newline separate entries can be provided for collecting multiple regions
  * If unspecified, all regions will be collected

## Docker-compose

1. Copy the example file and add your project id to it
1. Change the volume to point to your credentials file if different
1. Run `docker-compose up` and you'll have a prometheus instance running at http://localhost:9090 and a gcp-quota-exporter instance running at http://localhost:9592.

## Docker

### Local Build

```
docker build -t gcp-quota-exporter .
docker run -it --rm -v $(pwd)/credentials.json:/app/credentials.json -e GOOGLE_APPLICATION_CREDENTIALS=/app/credentials.json -e GOOGLE_PROJECT_ID=project_id gcp-quota-exporter
```

### Official Build

```
docker run -it --rm -v $(pwd)/credentials.json:/app/credentials.json -e GOOGLE_APPLICATION_CREDENTIALS=/app/credentials.json -e GOOGLE_PROJECT_ID=project_id mintel/gcp-quota-exporter
```

## History

In June 2022, [mintel/gcp-quota-exporter](https://github.com/mintel/gcp-quota-exporter) was archived in read-only mode and no longer maintained. In July 2023,
the project was forked and extended to support multiple GCP services beyond Compute Engine.
