package main

import (
	"os"
	"testing"

	promlog "github.com/prometheus/common/promlog"
)

// nolint: paralleltest
func TestScrape(t *testing.T) {
	logger := promlog.New(&promlog.Config{})
	tempFile, err := os.CreateTemp("", "test-credentials-*.json")

	if err != nil {
		t.Error(err)
	}

	defer os.Remove(tempFile.Name())

	jsonBlob := []byte(`{"type":"service_account","project_id":"test_project_id"}`)
	if _, err := tempFile.Write(jsonBlob); err != nil {
		t.Error(err)
	}

	t.Setenv("GOOGLE_APPLICATION_CREDENTIALS", tempFile.Name())

	// TestFailedConnection
	// Set the project name to "503" since the Google Compute API will append this to the end of the BasePath
	exporter, _ := NewExporter([]string{"503"}, []string{"us-east1"}, logger)
	exporter.service.BasePath = "http://httpstat.us/"
	projectUp, regionsUp := exporter.scrape("503")

	if projectUp != nil {
		t.Errorf("TestFailedConnection: projectUp=1, expected=0")
	}

	if regionsUp != nil {
		t.Errorf("TestFailedConnection: regionsUp=1, expected=0")
	}
}
