// Package main implements executable commands.
package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/compute/metadata"
	"github.com/PuerkitoBio/rehttp"
	kingpin "github.com/alecthomas/kingpin/v2"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	promlog "github.com/prometheus/common/promlog"
	promlogflag "github.com/prometheus/common/promlog/flag"
	"github.com/prometheus/common/version"
	"golang.org/x/oauth2/google"
	compute "google.golang.org/api/compute/v1"
	"google.golang.org/api/option"

	"github.com/tidwall/gjson"
)

var (
	limitDesc          = prometheus.NewDesc("gcp_quota_limit", "quota limits for GCP components", []string{"project", "region", "metric"}, nil)
	usageDesc          = prometheus.NewDesc("gcp_quota_usage", "quota usage for GCP components", []string{"project", "region", "metric"}, nil)
	projectQuotaUpDesc = prometheus.NewDesc("gcp_quota_project_up", "Was the last scrape of the Google Project API successful.", []string{"project"}, nil)
	regionsQuotaUpDesc = prometheus.NewDesc("gcp_quota_regions_up", "Was the last scrape of the Google Regions API successful.", []string{"project"}, nil)

	gcpProjectIDs = kingpin.Flag(
		"gcp.project_id", "IDs of the Google Projects to be monitored. Repeat for multiple projects. For environment variable $GOOGLE_PROJECT_ID, use newline delimited values for multiple projects.", // nolint: lll
	).Envar("GOOGLE_PROJECT_ID").Strings()

	gcpRegions = kingpin.Flag(
		"gcp.region", "Region to collect metrics for. Repeat for multiple regions. If left unset, all available regions will be collected.",
	).Envar("GOOGLE_REGION").Strings()

	gcpMaxRetries = kingpin.Flag(
		"gcp.max-retries", "Max number of retries that should be attempted on 503 errors from gcp. ($GCP_EXPORTER_MAX_RETRIES)",
	).Envar("GCP_EXPORTER_MAX_RETRIES").Default("0").Int()

	gcpHTTPTimeout = kingpin.Flag(
		"gcp.http-timeout", "How long should gcp_exporter wait for a result from the Google API ($GCP_EXPORTER_HTTP_TIMEOUT)",
	).Envar("GCP_EXPORTER_HTTP_TIMEOUT").Default("10s").Duration()

	gcpMaxBackoffDuration = kingpin.Flag(
		"gcp.max-backoff", "Max time between each request in an exp backoff scenario ($GCP_EXPORTER_MAX_BACKOFF_DURATION)",
	).Envar("GCP_EXPORTER_MAX_BACKOFF_DURATION").Default("5s").Duration()

	gcpBackoffJitterBase = kingpin.Flag(
		"gcp.backoff-jitter", "The amount of jitter to introduce in a exp backoff scenario ($GCP_EXPORTER_BACKODFF_JITTER_BASE)",
	).Envar("GCP_EXPORTER_BACKOFF_JITTER_BASE").Default("1s").Duration()

	gcpRetryStatuses = kingpin.Flag(
		"gcp.retry-statuses", "The HTTP statuses that should trigger a retry ($GCP_EXPORTER_RETRY_STATUSES)",
	).Envar("GCP_EXPORTER_RETRY_STATUSES").Default("503").Ints()
)

// Exporter collects quota stats from the Google Compute API and exports them using the Prometheus metrics package.
type Exporter struct {
	service  *compute.Service
	projects []string
	regions  []string
	mutex    sync.RWMutex
	logger   log.Logger
}

// scrape connects to the Google API to retrieve quota statistics and record them as metrics.
func (e *Exporter) scrape(projectID string) (prj *compute.Project, rgl []*compute.Region) {
	project, err := e.service.Projects.Get(projectID).Do()
	if err != nil {
		_ = level.Error(e.logger).Log("msg", "Failure when querying project quotas", "error", err)

		project = nil
	}

	regionList := e.getRegionList(projectID)

	return project, regionList
}

// Describe is implemented with DescribeByCollect. That's possible because the
// Collect method will always return the same metrics with the same descriptors.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(e, ch)
}

// Collect will run each time the exporter is polled and will in turn call the
// Google API for the required statistics.
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.mutex.Lock() // To protect metrics from concurrent collects.
	defer e.mutex.Unlock()

	for _, p := range e.projects {
		e.scrapeProject(ch, p)
	}
}

func (e *Exporter) getRegionList(projectID string) (regionList []*compute.Region) {
	var allRegions []*compute.Region

	regionsCall, err := e.service.Regions.List(projectID).Do()
	if err != nil {
		_ = level.Error(e.logger).Log("msg", "Failure when querying region quotas", "error", err)
		return nil
	}

	allRegions = append(allRegions, regionsCall.Items...)

	if len(e.regions) > 0 {
		// Populate user supplied regions into map
		selectedRegions := make(map[string]bool)
		for _, selectedRegion := range e.regions {
			selectedRegions[selectedRegion] = true
		}

		// Check all GCP regions against user supplied values
		for _, region := range allRegions {
			if selectedRegions[region.Name] {
				regionList = append(regionList, region)
			}
		}
	} else {
		return allRegions
	}

	return regionList
}

func (e *Exporter) scrapeProject(ch chan<- prometheus.Metric, projectID string) {
	project, regionList := e.scrape(projectID)

	if project != nil {
		for _, quota := range project.Quotas {
			ch <- prometheus.MustNewConstMetric(limitDesc, prometheus.GaugeValue, quota.Limit, projectID, "", quota.Metric)
			ch <- prometheus.MustNewConstMetric(usageDesc, prometheus.GaugeValue, quota.Usage, projectID, "", quota.Metric)
		}
		ch <- prometheus.MustNewConstMetric(projectQuotaUpDesc, prometheus.GaugeValue, 1, projectID)
	} else {
		ch <- prometheus.MustNewConstMetric(projectQuotaUpDesc, prometheus.GaugeValue, 0, projectID)
	}

	if regionList != nil {
		for _, region := range regionList {
			regionName := region.Name

			for _, quota := range region.Quotas {
				ch <- prometheus.MustNewConstMetric(limitDesc, prometheus.GaugeValue, quota.Limit, projectID, regionName, quota.Metric)
				ch <- prometheus.MustNewConstMetric(usageDesc, prometheus.GaugeValue, quota.Usage, projectID, regionName, quota.Metric)
			}
		}
		ch <- prometheus.MustNewConstMetric(regionsQuotaUpDesc, prometheus.GaugeValue, 1, projectID)
	} else {
		ch <- prometheus.MustNewConstMetric(regionsQuotaUpDesc, prometheus.GaugeValue, 0, projectID)
	}
}

// NewExporter returns an initialised Exporter.
func NewExporter(projects []string, regions []string, logger log.Logger) (*Exporter, error) {
	// Create context and generate compute.Service
	ctx := context.Background()

	googleClient, err := google.DefaultClient(ctx, compute.ComputeReadonlyScope)
	if err != nil {
		return nil, fmt.Errorf("error creating google client: %w", err)
	}

	googleClient.Timeout = *gcpHTTPTimeout
	googleClient.Transport = rehttp.NewTransport(
		googleClient.Transport, // need to wrap DefaultClient transport
		rehttp.RetryAll(
			rehttp.RetryMaxRetries(*gcpMaxRetries),
			rehttp.RetryStatuses(*gcpRetryStatuses...)), // Cloud support suggests retrying on 503 errors
		rehttp.ExpJitterDelay(*gcpBackoffJitterBase, *gcpMaxBackoffDuration), // Set timeout to <10s as that is prom default timeout
	)

	computeService, err := compute.NewService(ctx, option.WithHTTPClient(googleClient))
	if err != nil {
		_ = level.Error(logger).Log("Unable to create service", err)

		os.Exit(1)
	}

	return &Exporter{
		service:  computeService,
		projects: projects,
		regions:  regions,
		logger:   logger,
	}, nil
}

func getProjectIDFromMetadata() (string, error) {
	client := metadata.NewClient(&http.Client{})

	projectID, err := client.ProjectID()
	if err != nil {
		return "", fmt.Errorf("error: %w", err)
	}

	return projectID, nil
}

// nolint: funlen
func main() {
	var (
		listenAddress = kingpin.Flag("web.listen-address", "Address to listen on for web interface and telemetry.").Default(":9592").String()
		metricsPath   = kingpin.Flag("web.telemetry-path", "Path under which to expose metrics.").Default("/metrics").String()
		basePath      = kingpin.Flag("test.base-path", "Change the default googleapis URL (for testing purposes only).").Default("").String()
		promlogConfig promlog.Config
	)

	promlogflag.AddFlags(kingpin.CommandLine, &promlogConfig)
	kingpin.Version(version.Print("gcp_quota_exporter"))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	logger := promlog.New(&promlogConfig)

	_ = level.Info(logger).Log("msg", "Starting gcp_quota_exporter", "version", version.Info())
	_ = level.Info(logger).Log("Build Context", version.BuildContext())

	// Detect a Project ID from application credentials
	if len(*gcpProjectIDs) == 0 { // nolint: nestif
		credentialsFile := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")

		if credentialsFile != "" {
			c, err := os.ReadFile(credentialsFile)
			if err != nil {
				_ = level.Error(logger).Log("msg", "Unable to read credentials file",
					"file", credentialsFile, "error", err)

				os.Exit(1)
			}

			projectID := gjson.GetBytes(c, "project_id")

			if projectID.String() == "" {
				_ = level.Error(logger).Log("msg", "Could not retrieve Project ID from credentials file", "file", credentialsFile)

				os.Exit(1)
			}

			*gcpProjectIDs = []string{projectID.String()}
		} else {
			projectID, err := getProjectIDFromMetadata()
			if err != nil {
				_ = level.Error(logger).Log("Error looking up project from metadata", err)

				os.Exit(1)
			}

			*gcpProjectIDs = []string{projectID}
		}
	}

	if len(*gcpProjectIDs) == 0 {
		_ = level.Error(logger).Log("msg", "GCP Project IDs cannot be empty")

		os.Exit(1)
	}

	exporter, err := NewExporter(*gcpProjectIDs, *gcpRegions, logger)
	if err != nil {
		_ = level.Error(logger).Log("error", err)

		os.Exit(1)
	}

	if *basePath != "" {
		exporter.service.BasePath = *basePath
	}

	prometheus.MustRegister(exporter)
	prometheus.MustRegister(version.NewCollector("gcp_quota_exporter"))

	_ = level.Info(logger).Log("Google Projects: ", strings.Join(*gcpProjectIDs, ", "))
	_ = level.Info(logger).Log("msg", "Listening", "address", *listenAddress)

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err = w.Write([]byte(`<html>
							<head><title>GCP Quota Exporter</title></head>
							<body>
							<h1>GCP Quota Exporter</h1>
							<p><a href='` + *metricsPath + `'>Metrics</a></p>
							</body>
							</html>`))
	})

	server := &http.Server{
		Addr:              *listenAddress,
		ReadHeaderTimeout: 5 * time.Second,
	}

	err = server.ListenAndServe()
	_ = level.Error(logger).Log("error", err)

	os.Exit(1)
}
